# Ki365
Ki365 is an attempt to create something similar to Altium365 for Kicad.

## Features
- [x] Component database (accessible from kicad as a database library)
  - [x] Multiple manufacturers for individual parts
  - [x] Multiple suppliers for each manufacturer part
- [x] Assemblies
  - [ ] BOM export/import from web UI
  - [x] BOM export/import from commandline/CI/CD
  - [ ] BOM cost estimation

## Feature wishlist
- FreeCad integration/part database
- Gitlab/Github integration
- Ideas? [Open a Issue](https://gitlab.com/ki365/ki-365/-/issues/new?issue[title]=%5BFeature%5D)

## Anti-features
- Project-storage / collaboration  
  Use Gitlab/Github/whatever you prefer instead


## Demo
[Link to Ki365 Demo](https://demo.ki365.org)  
Username is `Admin` with password `demo`

## Screenshots

### Part library
![part_view](./screenshots/parts_view.png)  

### Assemblies
![](./screenshots/assembly_view.png)  
![](./screenshots/assembly_revision_view.png)  

### Kicad Integration
![](./screenshots/add_edit_kicad.png)  
![](./screenshots/kicad_select_symbol.png)  
![](./screenshots/kicad_database_browser.png)  
![](./screenshots/kicad_symbol_browser.png)  
![](./screenshots/kicad_footprint_browser.png)  

